# dumpe2fs - dump ext2/ext3/ext4 filesystem information
sudo dumpe2fs -h /dev/sda1 # 查看superblock和inode等文件系统信息

# mtr - a network diagnostic tool
sudo mtr --report code.eoe.cn # 查看访问code.eoe.cn的网络链路情况

# from mongodb meeting
atop
htop
blockdev
dstat
lsblk

# tmpfs文件系统 padding factor


# 进程
pstree



# 测试web并发性能
ab
siege